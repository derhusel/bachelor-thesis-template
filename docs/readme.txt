Diese LaTeX-Vorlage wurde dankenswerterweise von Studierenden speziell für Bachelorarbeiten am Studiengang ITS erstellt.
Es besteht keine Garantie oder Gewähr bzgl. der 100%igen Einhaltung formaler Kriterien.
Beachten Sie, dass es studiengangsseitig keine konkrete Unterstützung für diese Vorlage oder die Verwendung von LaTeX im Allgemeinen gibt.
WICHTIG: Zum Kompilieren wird zwingend XeLatex benötigt.
