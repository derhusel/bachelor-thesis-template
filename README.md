# Bachelor Thesis Template
> This is a template for a bachelor thesis at ITS (FHSalzburg)

[![pipeline status](https://gitlab.com/derhusel/bachelor-thesis-template/badges/master/pipeline.svg)](https://gitlab.com/derhusel/bachelor-thesis-template/commits/master)
[![License](http://img.shields.io/:license-mit-blue.svg)](http://badges.mit-license.org)

## Table of Contents (Optional)

 - [Installation](#installation)
 - [Usage](#usage)
 - [Features](#features)
 - [License](#license)


## Installation

### Install Ubuntu/Debian LaTeX Enviroment
 1. Update & Upgrade local machine `sudo apt-get update && sudo apt-get upgrade -y`
 2. Install TexLive Distribution with `sudo apt-get install texlive texlive-latex-extra texlive-generic-extra texlive-xetex`
 3. Since the template uses the minted package, we need to install the python package pygments `sudo apt-get install python3-pygments`

### Install Windows LaTeX Enviroment
 1. Install MikTex from https://miktex.org/download
 2. Install Perl from http://strawberryperl.com/
 3. Install Python on Windows https://realpython.com/installing-python/
 4. Run `pip3 install pygments --user` in ps/cmd window for the minted package

### Clone
Clone this repository with `git clone https://gitlab.com/derhusel/bachelor-thesis-template` to your local machine.


## Usage

### Compile in console/powershell/cmd
 1. Write your LaTeX document
 2. Compile it with `latexmk -xelatex-shell-escape FHSTemplate.tex`
 3. The compiled pdf should land in your directory

### Compile with VS Code and LaTeX Workshop Extension

 1. Install VS Code from https://code.visualstudio.com/
 2. Install Extension LaTeX Workshop in VS Code
 3. Edit you settings as json file and add the folowing configuration
    ```json
    "latex-workshop.latex.recipes": [
        {
            "name": "latexmk",
            "tools": [
                "latexmk"
            ]
        }
    ],
    "latex-workshop.latex.tools": [
        {
            "name": "latexmk",
            "command": "latexmk",
            "args": [
                "-xelatex",
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-shell-escape",
                "%DOC%"
            ]
        }
    ],
    "latex-workshop.view.pdf.viewer": "tab",
    ```
 4. After opening a .tex file in VS Code you should have a TeX symbol at the left panel
 5. Open the symbol and go to Build LaTeX project
 6. Build it with the recipe LaTeXmk
 7. The compiled document should land in your directory

### Compile using the GitLab CI
> Either have a free GitLab.com account with 2000 free CI pipeline minutes per month or install a GitLab Runner on a local machine https://docs.gitlab.com/runner/install/
 1. Push your project to a gitlab repository
 2. The CI should automatically start and compile your project
 3. The compiled document can be found at the artifacts of the pipeline after the completion


## Features
 - automatic compilation with GitLab CI
 - compilation with VS Code and LaTeXWorkshop
 - syntax highlighting with minted/pygmentize
 - template for FH Salzburg ITS Bachelor thesis
 - Syntax templates for LaTeX

## License

[![License](http://img.shields.io/:license-mit-blue.svg)](http://badges.mit-license.org)

 - **[MIT license](http://opensource.org/licenses/mit-license.php)**